<!--
(b)---Indica que el texto debe ser representado con negrita.
(br)--Indica que queremos hacer un salto de línea normal.
(cite)--Marca una referencia a una fuente, o el autor de un texto citado.
(dfn)--Sirve para marcar el término que se quiere definir.
(em)--Marca con énfasis las partes importantes de un texto.
(i)--Muestra el texto marcado con un estilo en cursiva o italica.
(q)--Crea citas en linea, marca las citas a otros autores o documentos.
(small)--Representa comentarios al margen
(time)-- representa un periodo específico en el tiempo
-->
<html>
<head><title> Practica 02 parte 2</title></head>
<body>
<article>
<header>
<p>posted by BGB, November 15, 2012</p>
</header>
<h1>Low and Slow</h1>
<p>This week I am <i>extremely</i> excited about a new cooking
technique called sous vide. In <i>sous vide</i> cooking, you submerge
the food (usually vacuum-sealed in plastic) into a water bath
that is precisely set to the target temperature you want the
food to be cooked to. In his book, <i>Cooking for Geeks</i>, Jeff
Potter describes it as <dfn>ultra-low-temperature poaching.</dfn></p>
<p>Next month, we will be serving <b>Sous Vide Salmon with Dill
Hollandaise.</b> To reserve a seat at the chef table, contact us
before November 30.</p>
<p>blackgoose@example.com <br>555-336-1800</br></p>
<p><small> Warning: Sous vide cooked salmon is not pasteurized. Avoid
it if you are pregnant or have immunity issues.</small></p>
</article>
</body>
</html>

